package in.forsk;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Saurabh on 3/28/2016.
 */
public class Utils {

    private final static String TAG = Utils.class.getSimpleName();

    /**
     * Method to convert the Input stream to string.
     *
     * @param is
     * @return
     * @throws IOException
     */
    public static String convertStreamToString(InputStream is) throws IOException {
        // Converting input stream into string
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int i = is.read();
        while (i != -1) {
            baos.write(i);
            i = is.read();
        }
        return baos.toString();
    }


    /**
     * Method to read the json file from RAW folder.
     *
     * @param context
     * @param resourceId
     * @return
     * @throws IOException
     */
    public static String getStringFromRaw(Context context, int resourceId)
            throws IOException {
        // Reading File from resource folder
        Resources r = context.getResources();
        InputStream is = r.openRawResource(resourceId);
        String statesText = convertStreamToString(is);
        is.close();

        Log.d(TAG, statesText);

        return statesText;
    }
}
