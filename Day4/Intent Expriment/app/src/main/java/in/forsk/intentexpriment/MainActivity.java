package in.forsk.intentexpriment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    private Button firstBtn, secondBtn, thirdBtn, forthBtn, fifthBtn;

    private final static int REQUEST_CODE = 1;
    private final static int PICK_CONTACT = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        firstBtn = (Button) findViewById(R.id.button);
        secondBtn = (Button) findViewById(R.id.button2);
        thirdBtn = (Button) findViewById(R.id.button3);
        forthBtn = (Button) findViewById(R.id.button4);
        fifthBtn = (Button) findViewById(R.id.button5);

        firstBtn.setOnClickListener(this);
        secondBtn.setOnClickListener(this);
        thirdBtn.setOnClickListener(this);
        forthBtn.setOnClickListener(this);
        fifthBtn.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.button:
                Toast.makeText(MainActivity.this, "First Button", Toast.LENGTH_LONG).show();
                Log.e("First Button", "Log from First Button");

                Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                intent.putExtra("BtnName","Comming from Button One");
                startActivity(intent);

                break;
            case R.id.button2:
                Toast.makeText(MainActivity.this, "Second Button", Toast.LENGTH_LONG).show();
                Log.e("Second Button", "Log from Second Button");

                Intent intent1 = new Intent(MainActivity.this, SecondActivity.class);
                intent1.putExtra("BtnName", "Testing StartActivityForResult");
                startActivityForResult(intent1,REQUEST_CODE);
                break;
            case R.id.button3:
                Toast.makeText(MainActivity.this, "Third Button", Toast.LENGTH_LONG).show();
                Log.e("Third Button", "Log from Third Button");

                Intent google_intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://google.com"));
                startActivity(google_intent);

                break;
            case R.id.button4:
                Toast.makeText(MainActivity.this, "Forth Button", Toast.LENGTH_LONG).show();
                Log.e("Forth Button", "Log from Forth Button");

                Intent send_intent = new Intent(Intent.ACTION_SEND);
                send_intent.setType("text/plain");
                send_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                send_intent.putExtra(android.content.Intent.EXTRA_TEXT, "Standing on the Moon!");
//                startActivity(send_intent);

                Intent chooser = Intent.createChooser(send_intent, "Chooser Title!");

                if (chooser.resolveActivity(getPackageManager()) != null) {
                    startActivity(chooser);
                }

                break;
            case R.id.button5:
                Toast.makeText(MainActivity.this, "Fifth Button", Toast.LENGTH_LONG).show();
                Log.e("Fifth Button", "Log from Fifth Button");

//                Intent callIntent = new Intent(Intent.ACTION_CALL);
//                callIntent.setData(Uri.parse("tel:0377778888"));
//                startActivity(callIntent);

//                Intent pick_intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
//                startActivityForResult(pick_intent,PICK_CONTACT );

//                // Create a Uri from an intent string. Use the result to create an Intent.
//                Uri gmmIntentUri = Uri.parse("google.streetview:cbll=46.414382,10.013988");
//
//                // Create an Intent from gmmIntentUri. Set the action to ACTION_VIEW
//                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
//
//                // Make the Intent explicit by setting the Google Maps package
//                mapIntent.setPackage("com.google.android.apps.maps");
//
//                // Attempt to start an activity that can handle the Intent
//                if (mapIntent.resolveActivity(getPackageManager()) != null) {
//                    startActivity(mapIntent);
//                }


//                Intent camera_intent = new Intent("android.media.action.IMAGE_CAPTURE");
//                if (camera_intent.resolveActivity(getPackageManager()) != null) {
//                    startActivity(camera_intent);
//                }

                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_CODE){
            if(resultCode == RESULT_OK){
                String result = data.getStringExtra("result");
                Log.e("Result", result);
                secondBtn.setText(result);
            }

            if(resultCode == RESULT_CANCELED){
                Log.e("Result", "Request Cancelled");
            }
        }


    }

    public void thirdButtonClick(View v) {
        Toast.makeText(MainActivity.this, "Third Button Toast Method", Toast.LENGTH_LONG).show();
    }
}
